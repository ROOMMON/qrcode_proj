import React, { Component } from 'react'
import axios from 'axios';

// const getApis = {
export const postNameList = async (name, phone, uid, numOfGroup, tableNum, detail) => {

    console.info("[start] post visitor data")

    await axios.post('https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/userData?uid=' + uid, {
        visitors: [
            {
                name: name,
                phone: phone,
                numOfGroup: numOfGroup,
                tableNum: tableNum,
                detail: detail
            }
        ]
    })
    .then(res => {
        console.log(res.data)
        console.info("[end] post visitor data")
    })
    .catch(e => console.error(e))
}

export const postRestaurantURL = async (url, nameOfPlace, hashedNameOfPlace) => {

    console.log("[Start] generate qr code and post DB")

    await axios.post('https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/qrcodeGenerate', {
        url: url,
        nameOfPlace: nameOfPlace,
        hashedNameOfPlace: hashedNameOfPlace
    })
    .then(res => {
        console.log(res.data)
        console.log("[End] generate qr code and post DB")
    })
    .catch(e => console.error(e))

}

export const enqueue = async (name, phone, numberOfPeople=1, uid) => {
    console.log("[Start] enqueue")
    const res = await axios.post(
        'https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/enqueue?uid=' + uid, 
        {name: name, phone: phone, numberOfPeople:numberOfPeople}
    ).catch(e => console.error(e))
    return res? res.data : res;
}

export const dequeueByIndex = async (index, uid) => {
    console.log("[Start] dequeue")
    const res = await axios.post(
        'https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/enqueue?uid=' + uid, 
        {index: index}
    ).catch(e => console.error(e))
    return res? res.data : res;
}

// export const postLogin = async (email, password) => {
//     console.log("[Start] login")

//     await axios.post('/login', {
//         email: email,
//         password: password
//     })
//     .then(res => {
//         console.log(res.data)
//         console.log("[End] login")
//     })
//     .catch(e => console.error(e))
// }
// }

// export default getApis;  