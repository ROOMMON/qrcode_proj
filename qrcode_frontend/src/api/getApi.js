import React, { Component } from 'react'
import axios from 'axios';

// const getApis = {
export const getNameList = async (uid) => {

    console.log("[Start] getNameList")
    let data
    await axios.get('https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/userData?uid=' + uid)
    .then(res => {
        
        
        console.log(res.data)
        console.log("[End] getNameList")
        data = res.data
    })
    .catch(e => console.error(e))

    return data
}

export const getQueue = async (uid) => {

    console.log("[Start] getQueue")
    let data
    await axios.get(
        'https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/queue?uid=' + uid )
        .then((res) => {
            
         console.log("[End] getQueue", res.data)
         data = res.data
        })
        .catch(e => console.error(e))

    
    return data
}

export const dequeue = async (uid) => {

    console.log("[Start] dequeue")

    const res = await axios.get(
        'http://localhost:5000/qrcode-4b0f5/us-central1/api/dequeue/?uid=' + uid )
        .catch(e => console.error(e))
    console.log("[End] dequeue", res.data)
    return res.data
}

// }

// export default getApis;