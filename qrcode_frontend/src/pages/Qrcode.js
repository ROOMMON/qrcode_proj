import React, { Component, useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import {postRestaurantURL} from '../api/postApi'
import '../styles/mainStyle.css'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import { useLocation, useHistory  } from 'react-router-dom'

const bcrypt = require("bcryptjs")

// var React = require('react');
var QRCode = require('qrcode.react');

const Qrcode = (props) => {

    let baseUrl = "https://qrcode-4b0f5.web.app/";

    const classes = useStyles();
    const location = useLocation()

    const queryUrl = location.search

    const [nameOfPlace, setNameOfPlace] = useState("")
    const [url, setUrl] = useState("")
    const [isPressed, setIsPressed] = useState(false)
    const [hashedNameOfPlace, setHashedNameOfPlace] = useState("")
    const [numOfTable, setNumOfTable] = useState("")

    const [handleMode, setHandleMode] = useState(true)


    const MyButton = (props) => {
        const { color, ...other } = props;
        const classes = useStyles(props);
        return <Button className={classes.button} {...other} />;
    }

    MyButton.propTypes = {
        color: PropTypes.oneOf(['blue', 'red']).isRequired,
    };

    useEffect(()=>{
        console.log(1)
        // bcrypt.genSalt(10, (err, salt) => {
        //     bcrypt.hash(nameOfPlace, salt, (err, hash) => {
        //     if (err) throw err;
        //     setHashedNameOfPlace(hash);
        //     setUrl(baseUrl + "?nameOfPlace=" + hashedNameOfPlace) 
        //     });
        //     console.log(url)
        //     if(url !== "" && nameOfPlace !== "") {
        //         postRestaurantURL(url, nameOfPlace, hashedNameOfPlace)
        //     }
        // });
        setUrl(window.location.origin + queryUrl)
            console.log("url=",url)
    },[])

    useEffect(() => {
        if(url != "") {
            console.log("url=",url)
        }
    }, [url])

    const onBtnClick = () => {

        if(isPressed === true) {
            setIsPressed(false)
            setUrl("")
        } else if(nameOfPlace === "") {
            alert("Place name is missing!")
        } else if(isPressed === false) {
            setIsPressed(true)
            setUrl(window.location.origin + queryUrl)
            console.log("url=",url)
        } 
    }
    
    const onHandleMode= () => {
        if( handleMode === true ) {
            setHandleMode(false)
        } else {
            setHandleMode(true)
        }
    }

    return (
        <>
            <div className="mainBox">
                <div style={{width:"35%", display:"flex", flexDirection:"row", flexWrap:"wrap", justifyContent:"center", alignItems:"center"}}>
                {handleMode ? (
                    <>
                        <div>
                            <MyButton onClick={onHandleMode} >Generate QRcode</MyButton>
                        </div>
                        {url !== "" ? (
                            <div>
                                {console.log('url=', url, "qrcode created!")}
                                <QRCode size={290} value={url} />
                            </div>
                            
                        ) : null}
                    
                    </>
                    ) : (
                        <>
                        <Grid>
                            <TextField 
                                label="Table Number"
                                value={nameOfPlace}
                                onChange={(e) => setNumOfTable(e.target.value)}
                                id="numOfTable"
                                variant='standard'
                                helperText="*Required"
                                className={classes.textField}
                                FormHelperTextProps=
                                    {{style: {color: 'blue'}}}
                            />
                        </Grid>
                        {isPressed ? (
                            <>
                                <div style={{display: "flex", flexDirection:"row", flexWrap: "wrap", justifyContent: "center", alignItems:"center"}}>
                                    <div>
                                        <MyButton onClick={onBtnClick} >Cancel</MyButton>
                                    </div>
                                </div>
                                
                            </>
                        ) : (
                            <div>
                                <MyButton onClick={onBtnClick} >Generate QRcode</MyButton>
                            </div>
                        )}
                        </>
                    )
                        }
                

                </div>
            </div>        
        </>
    )

    // return (
    //     <QRCode
    //       value={this.state.value}
    //       size={this.state.size}
    //       fgColor={this.state.fgColor}
    //       bgColor={this.state.bgColor}
    //       level={this.state.level}
    //       renderAs={this.state.renderAs}
    //       includeMargin={this.state.includeMargin}
    //       imageSettings={
    //         this.state.includeImage
    //           ? {
    //               src: this.state.imageSrc,
    //               height: this.state.imageH,
    //               width: this.state.imageW,
    //               x: this.state.centerImage ? null : this.state.imageX,
    //               y: this.state.centerImage ? null : this.state.imageY,
    //               excavate: this.state.imageExcavate,
    //             }
    //           : null
    //       }
    //     />
    // )
}

const useStyles = makeStyles((theme) => ({
    container: {
      height: "calc(100vh - 80px)",
      width: "100%",
      flexWrap: 'wrap',
      display: 'grid',
      justifyContent: 'center',
      alignItems: 'center',
      direction: 'row'
    },
    button: {
        background: 'linear-gradient(45deg, #33c9dc 30%, #039be5 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(51, 201, 220 .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: 8,
      },
    textField: {
        width: '300px'
    }
  }));

export default Qrcode;