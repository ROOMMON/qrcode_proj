import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles';
import '../styles/mainStyle.css'
import {postNameList} from '../api/postApi'
import Button from '@material-ui/core/Button';
import PropTypes, { resetWarningCache } from 'prop-types';
import Typography from '@material-ui/core/Typography'
import { useLocation  } from 'react-router-dom'

const Register = (props) => {
    
    const classes = useStyles();
    const location = useLocation()

    const [nameValue, setNameValue] = useState("")
    const [phoneNum, setPhoneNum] = useState("")
    const [numOfPeople, setNumOfPeople] = useState("")
    const [friendsName, setFriendsName] = useState({})
    // const [friendPhone, setFriendPhone] = useState([])

    const [param, setParam] = useState("")
    const [uid, setUid] = useState("")
    const [nameOfPlace, setNameOfPlace] = useState("")
    const [tableNum, setTableNum] = useState("")

    //Error handeling
    const [error, setError] = useState({})
    const [friendsError, setFriendsError] = useState({})
    
    
    let urlPath = location.search

    const MyButton = (props) => {
        const { color, ...other } = props;
        const classes = useStyles(props);
        return <Button type='submit' variant='contained' className={classes.button} {...other} />;
    }
    
    MyButton.propTypes = {
        color: PropTypes.oneOf(['blue', 'red']).isRequired,
    }

    useEffect(()=>{
        console.log(urlPath)
        let queryUrl = new URLSearchParams(urlPath)
        setParam(new URLSearchParams(urlPath).get('nameOfPlace'))
        console.log(queryUrl.get('tableNum'))
        console.log(queryUrl.get('barNum'))
        let num
        if(queryUrl.get('tableNum') != null) {
            num = queryUrl.get('tableNum')
            setTableNum('Table ' + num)
        } else if(queryUrl.get('barNum') != null) {
            num = queryUrl.get('barNum')
            setTableNum('Bar ' + num)
        }

        // setTableNum(new URLSearchParams(urlPath).get('tableNum'))
    },[])

    useEffect(()=>{
        if(param != null && param != "") {
            console.log("!#!#!@#", param)
            const splitedParam = param.split(' ')
            setUid(splitedParam[1])
            setNameOfPlace(splitedParam[0])
            console.log("uid === ", splitedParam[1])
            console.log("nameOfPlace === ", splitedParam[0])
        }
    }, [param])

    useEffect(()=>{
        console.log(friendsError)
    },[friendsError])

    const resetInpuData = () => {
        setNameValue("")
        setPhoneNum("")
        setNumOfPeople("")
        setFriendsName({})
        setParam("")
        setFriendsError({})
    }
    const onBtnClick = () => {
        setError({})
        
        if(nameValue !== "" && phoneNum !== "" && numOfPeople !== "" ){
            console.log("firebase is called?")
            try {
                console.log("####################################")
                console.log("nameValue= ", nameValue)
                console.log("phoneNum= ", phoneNum)
                console.log("uid= ", uid)
                console.log("numOfPeople= ", numOfPeople)
                console.log("tableNum= ", tableNum)
                console.log("detail= ", friendsName)
                console.log("####################################")
                
                postNameList(nameValue, phoneNum, uid, numOfPeople, tableNum, friendsName)
                .then(() => {
                    resetInpuData()
                    alert("Submitted")
                })
            } catch(e) {
                console.error("error= ", e);
            }

        } else {
            if(nameValue === ""&& phoneNum === "") {
                setError({
                    name: "Must not be empty!",
                    phone: "Must not be empty"
                })
            } else if(nameValue === "") {
                setError({ name: "Must not be empty "})
            } else if(phoneNum === "") {
                setError({ phone: "Must not be empty "})
            }

            // for(var i = 1; i < numOfPeople; i++) {
            //     let obj = {[i]: "Must not be empty"}
                
            //     console.log(friendsName)
            //     console.log("friendsName[", i, ']=',friendsName[i])
            //     if(friendsName[i] == null || friendsName[i] == "") {

            //         console.log("@#$@#$@#$1")
            //         console.log(obj)
            //         console.log('friendsName=', friendsName)
            //         console.log('friendsError=', friendsError)
            //         console.log('index = ', i)
            //         console.log("@#$@#$@#$2")

            //         setFriendsError({
            //             ...friendsError,
            //             ...obj
            //         })
            //     }
            // }
            let err = {}
            for(var i =1 ; i < numOfPeople; i++) {
                
                let errObj = {
                    [i]: "Must not be empty"
                }
                if(friendsName[i] == null || friendsName[i] == "") {
                    console.log(i.toString() + "+_+_+")
                    err={
                        ...err,
                        ...errObj
                    }
                }
            }
            setFriendsError(err)
        }
    }
    
    const setFriendsNames = (index, e) => {
        let value = e.target.value
        let obj = {[`${index}`]: value}

        setFriendsName({
            ...friendsName,
            ...obj
        })

        console.log('index = ', index, 'e = ', e.target.value)
        console.log(friendsName)
        console.log(friendsError)
    }
    
    const createTextField = (numOfField) => {
        let fields = []
            //자기자신은 뺴야되니까
            for(var i =0 ; i< numOfField - 1; i++) {
                fields.push(
                    <div style={{width:"300px", display:"flex", flexDirection:"row", flexWrap:"nowrap", justifyContent:"space-between", alignItems:"center"}}>
                        <TextField 
                            marginRight='100'
                            label={"Name of friend " + (i+1).toString()}
                            value={friendsName[i+1]}
                            onChange={setFriendsNames.bind(this, i+1)}
                            id={"nameOfFriend" + (i+1).toString()}
                            helperText={friendsError != null && friendsError[i+1]  ? friendsError[i+1]: "(Optional)"}
                            error={friendsError != null && friendsError[i+1]  ? true : false}
                            variant='standard'
                            className={classes.detailTextField}
                            // FormHelperTextProps=
                            // {{ style: {color: 'blue'} }}
                        />
                        {/* <TextField 
                            marginRight='100'
                            label="Phone"
                            value={friendPhone[i]}
                            onChange={(e) => setFriendPhones(e.target.value, i)}
                            id="phoneValue"
                            helperText={error.phoneOfFriend ? error.phoneOfFriend : "(Optional)"}
                            error={error.phoneOfFriend ? true : false}
                            variant='standard'
                            className={classes.detailTextField}
                            // FormHelperTextProps=
                            //     {{ style: {color: 'blue'} }}
                        /> */}
                    </div>
                )
            }

        return fields
    }

    const textInput = 
            <div className="mainBox">
                {tableNum && (
                    <Typography variant="h6">
                    {tableNum}
                    </Typography>
                )}
                
                <h1>
                    <div style={{width:"100%", display:"flex", flexDirection:"row", flexWrap:"nowrap", justifyContent:"center", alignItems:"center", flexDirection: 'column'}}>
                        <Grid>
                            <TextField 
                                marginRight='100'
                                label="Name"
                                value={nameValue}
                                onChange={(e) => setNameValue(e.target.value)}
                                id="nameValue"
                                helperText={error.name ? error.name : "*Required"}
                                error={error.name ? true : false}
                                variant='standard'
                                className={classes.nameTextField}
                                FormHelperTextProps=
                                    {{ style: {color: 'blue'} }}
                            />
                        </Grid>
                        <Grid>
                            <div style={{width:"300px", display:"flex", flexDirection:"row", flexWrap:"nowrap", justifyContent:"space-between", alignItems:"center"}}>
                                <TextField 
                                    marginRight='100'
                                    label="Phone"
                                    value={phoneNum}
                                    onChange={(e) => setPhoneNum(e.target.value)}
                                    id="phoneValue"
                                    helperText={error.phone ? error.phone : "*Required"}
                                    error={error.phone ? true : false}
                                    variant='standard'
                                    className={classes.detailTextField}
                                    FormHelperTextProps=
                                        {{ style: {color: 'blue'} }}
                                />
                                <TextField 
                                    marginRight='100'
                                    label="Number of people"
                                    value={numOfPeople}
                                    onChange={(e) => setNumOfPeople(e.target.value)}
                                    id="numOfPeople"
                                    helperText={error.phone ? error.phone : "*Required"}
                                    error={error.phone ? true : false}
                                    variant='standard'
                                    className={classes.detailTextField}
                                    FormHelperTextProps=
                                        {{ style: {color: 'blue'} }}
                                />

                            </div>
                            {numOfPeople > 1 ? (
                                <div style={{display: 'flex', flexDirection: 'row',flexWrap:"nowrap", justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
                                    {createTextField(numOfPeople)}
                                </div>    
                                ) : (
                                    null
                                )}
                        </Grid>
                        <div>
                            <MyButton onClick={onBtnClick} color="red">Submit</MyButton>
                        </div>
                    </div>

                    
                </h1>
            </div>
            
    return  textInput
}

const useStyles = makeStyles((theme) => ({
    container: {
      height: "calc(100vh - 80px)",
      width: "100%",
      flexWrap: 'wrap',
      display: 'grid',
      justifyContent: 'center',
      alignItems: 'center',
      direction: 'row'
    },
    button: {
        background: 'linear-gradient(45deg, #33c9dc 30%, #039be5 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(51, 201, 220 .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: 8,
      },
    nameTextField: {
        width: '300px'
    },
    detailTextField: {
        width: '140px'
    }
  }));

export default Register;