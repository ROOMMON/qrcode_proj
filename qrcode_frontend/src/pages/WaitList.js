import React, { Component, useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid'
import '../styles/nameList.css'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import PropTypes from 'prop-types';
import {dequeueByIndex} from '../api/postApi';
import {getQueue} from '../api/getApi.js';

import { useLocation  } from 'react-router-dom'

const WaitList = (props) => {
    const classes = useStyles();
    const location = useLocation()

    const urlPath = location.search
    const [queue, setQueue] = useState([])
    const [ueryParam, setQueryParam] = useState("")
    const [nameOfPlace, setNameOfPlace] = useState("")
    const [uid, setUid] = useState("")

    
    
    const MyButton = (props) => {
        const { color, ...other } = props;
        const classes = useStyles(props);
        return <Button className={classes.button} {...other} />;
    }
    MyButton.propTypes = {
        color: PropTypes.oneOf(['blue', 'red']).isRequired,
    };
    useEffect(()=>{
        console.log(urlPath)
        console.log("@#$%@#$^@#$%#@$%@#$%#@%#$%@$#%")
        const param = new URLSearchParams(urlPath).get('nameOfPlace').split(" ")
        setUid(param[1])
        setNameOfPlace(param[0])
        console.log("uid === ", uid)
        console.log("nameOfPlace === ", nameOfPlace)
    })
    
    useEffect( () => {
        if(uid != "")
            fillQueueList(uid)
    }, [uid])

    const handleOwnerAction =async (i) =>{
        const res = await dequeueByIndex(i, nameOfPlace);
        if(res)
            setQueue(res.queue);
    }

    const fillQueueList = async (uid ) =>{
        const res = await getQueue(uid);
        console.log('res', res);
        setQueue(res);
    }

    const renderQueueList = () =>{
        const formatDate = (date, showSeconds=false) => {
            let d = new Date(date);
            let h = d.getHours();
            h = Number(h) < 10 ? '0'+ h : h;
            let m = d.getMinutes();
            m = Number(m) < 10 ? '0'+ m : m;
            let s = d.getSeconds();
            return showSeconds? `${h}:${m}:${s}` : `${h}:${m}`
        }
        let rows = [];
        if(queue)
        queue.forEach((queueObj, i)=>{
            rows.push(
                <>
                <div style={{display:"flex", flexDirection:"row", backgroundColor:i%2===0? '#ebebeb': '#fff'}}>
                    <div style={{width:'25%'}}>
                        {`#${i+1}`}
                    </div>
                    <div style={{width:'25%'}}>
                        {`${queueObj.name}(${queueObj.numberOfPeople? queueObj.numberOfPeople: 1})`}
                    </div>
                    <div style={{width:'25%'}}>
                    {queueObj.phone}
                    </div>
                    <div style={{width:'25%'}}>
                    {formatDate(queueObj.enqueueDate)}
                    </div>
                </div>
                <div style={{marginTop:'10px'}}>
                    <MyButton onClick={async ()=>{await handleOwnerAction(i, 'skip')}}>Skip</MyButton>
                    <MyButton onClick={async ()=>{await handleOwnerAction(i, 'accept')}} color="red">Accept customer</MyButton>
                    <MyButton onClick={()=>{}}>Send Message</MyButton>
                </div>
                </>
            );
        })
        return rows;
    }

    const handleViewMode = () =>{
        
            if(!(queue && queue.length))
            {
                return(
                    <div style={{width:"100vw", height:"calc(100vh - 80px)", paddingTop:'35px', boxSizing:"border-box"}}>
                        <h1>Queue is empty</h1>
                    </div>
                );
            }
            return(
                <div style={{width:"100vw", height:"calc(100vh - 80px)", paddingTop:'35px', boxSizing:"border-box"}}>
                    <div style={{marginBottom:'5px'}}>{`Next Person in the Queue:`}</div>
                    <div style={{fontSize:'24px', fontWeight:'500'}}>{queue && queue[0]? queue[0].name: ''}</div>
                    <div style={{fontSize:'18px', fontWeight:'500'}}>{queue && queue[0]? queue[0].phone: ''}</div>

                    <div style={{height:'50vh', width:'100%', overflowY: 'scroll', marginTop: '50px'}}>
                        {renderQueueList()}
                    </div>
                </div>
            );
        
    }

    return (
        handleViewMode()
    )
}

const useStyles = makeStyles((theme) => ({
    container: {
      height: "calc(100vh - 80px)",
      width: "100%",
      flexWrap: 'wrap',
      display: 'grid',
      justifyContent: 'center',
      alignItems: 'center',
      direction: 'row'
    },
    button: {
        background: 'linear-gradient(45deg, #33c9dc 30%, #039be5 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(51, 201, 220 .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: 8,
      },
    textField: {
        width: '300px'
    }
  }));

export default WaitList;