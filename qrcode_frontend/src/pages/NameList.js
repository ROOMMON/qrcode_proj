import React, { Component, useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid'
import '../styles/nameList.css'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import PropTypes from 'prop-types';
import {getNameList} from '../api/getApi'

import { BrowserRouter as Router, useHistory, Link, useLocation  } from 'react-router-dom'

const displayDetail = (detail) => {
  let arr = []
  if(detail != null) {
    for(var i = 0; i< Object.keys(detail).length; i++) {
      arr.push(<TableRow key={detail[i+1]}>
            <TableCell component="th" scope="row">
              {"Friend " + (i+1).toString() }
            </TableCell>
            <TableCell component="th" scope="row">
              {detail[i+1]}
            </TableCell>
          {/* <TableCell>{detailRow.phone}</TableCell> */}
        </TableRow>)
    }
  }
    
  
  return arr
}

const createData = (name, date, table, group, detail, phone, dateOfVisited) => {

    return {
      name,
      date,
      table,
      group,
      detail,
      phone,
      dateOfVisited
    };
  }

function Row(props) {
    const { row, users } = props;
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();
   
    useEffect(()=>{
        console.log("inside Row")
        console.log(row)
        console.log("inside Row")
    })


    return (
      <React.Fragment>
        <TableRow  className={classes.root}>
         <TableCell style={{paddingLeft: 16, paddingRight: 16, paddingTop: 5, paddingBottom: 5}}>
            <IconButton aria-label="expand row" size="small" onClick={() => {setOpen(!open)}}>
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton> 
          </TableCell>
          <TableCell 
          style={{minWidth: '50px',
                maxWidth: '50px',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
                overflow: 'hidden',
                padding: 1
                }}
                component="th" 
                scope="row">
            {row.name}
          </TableCell>
          <TableCell style={{padding: 8}} align="right">{row.date}</TableCell>
          <TableCell style={{padding: 8}} align="right">{row.table}</TableCell>
          <TableCell style={{paddingLeft: 16, paddingRight: 16, paddingTop: 1, paddingBottom: 1}} align="right">{row.group}</TableCell>
          {/* <TableCell align="right">{row.carbs}</TableCell>
          <TableCell align="right">{row.protein}</TableCell> */}
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box margin={1}>
                <Table size="small" aria-label="purchases">
                  <div style={{width:"100%", display:"flex", flexDirection:"row", flexWrap:"nowrap", justifyContent:"space-between", alignItems:"center"}}>
                    <div>
                    <Typography variant="h7" gutterBottom component="div">
                      Contact:
                    </Typography>
                    <TableBody>
                      <Typography variant="h7" gutterBottom component="div">
                      {row.phone}
                      </Typography>
                    </TableBody>
                    </div>
                    <div>
                    <Typography variant="h7" gutterBottom component="div">
                      Time:
                    </Typography>
                    <TableBody>
                      <Typography variant="h7" gutterBottom component="div">
                      {row.dateOfVisited}
                      </Typography>
                    </TableBody>
                    </div>
                  </div>
                </Table>
              </Box>
              <Box margin={1}>
                {row.detail[1] != null || row.detail[1] == "" ? (
                  <>
                    <Typography variant="h6" gutterBottom component="div">
                      Group
                    </Typography>
                    <Table size="small" aria-label="purchases">
                      <TableBody>
                        {/* {row.detail.map((detailRow) => ( */}
                          {displayDetail(row.detail)}
                        {/* ))} */}
                      </TableBody>
                    </Table>
                    </>
                ) : null }
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  }
  
  Row.propTypes = {
    row: PropTypes.shape({
      name: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      table: PropTypes.number.isRequired,
      group: PropTypes.number.isRequired,
      detail: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string.isRequired,
          phone: PropTypes.string.isRequired
        }),
      ).isRequired,
    }).isRequired,
  };
  
  
  const NameList = () => {
   
    const [users, setUsers] = useState([])
    const [rows, setRows] = useState([])
    let location = useLocation();

    let urlPath = location.search
    const param = new URLSearchParams(urlPath)
    const queryParam = param.get('nameOfPlace') ? param.get('nameOfPlace').split(" ") : [];
    const nameOfPlace = queryParam.length !== 0 ? queryParam[0] : ""
    const uid = queryParam.length !== 0 ? queryParam[1] : ""

    useEffect(()=>{
        loadNameList()
    }, [])

    useEffect(()=>{
        
        if(users != null && users.length > 0){
            createDataSet()
        }
        console.log(users)
    }, [users])


    const createDataSet =  () => {
        let dataList = []
        let date
        users.map((userRows)=>{
            date = new Date(userRows.dateOfVisited)
            let parsedDate = date.toString().split(" ")
            let firstName = userRows.name.split(" ")[0]

            dataList.push(createData(
                firstName, 
                parsedDate[1] + "-" + parsedDate[2],
                userRows.tableNum,
                userRows.numOfGroup,
                userRows.detail,
                userRows.phone,
                date.toLocaleTimeString()
            ))
        })
        setRows(dataList)
        console.log("[data set created!: ", rows)
    }

    const loadNameList = async () => {
        let data;
        data = await getNameList(uid)
        setUsers(data)
    }

    return (
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow >
              <TableCell />
              <TableCell style={{paddingLeft: 1, paddingBottom: 3}}>Name</TableCell>
              <TableCell style={{paddingBottom: 3}} align="right">Date</TableCell>
              <TableCell style={{paddingBottom: 3}}  align="right">Seat</TableCell>
              <TableCell style={{paddingBottom: 3}}  align="right">Group</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) =>(
              <Row key={row.name} row={row} users={users} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

const useStyles = makeStyles((theme) => ({
    container: {
    //   height: "calc(100vh - 80px)",
      width: "100%",
      flexWrap: 'wrap',
      display: 'grid',
      justifyContent: 'center',
      alignItems: 'center',
      direction: 'row'
    },
    button: {
        background: 'linear-gradient(45deg, #33c9dc 30%, #039be5 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(51, 201, 220 .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: 8,
      },
    textField: {
        width: '300px'
    },
    root: {
        '& > *': {
          borderBottom: 'unset',
        },
      },
  }));

export default NameList;