import React, {useState} from 'react'
import TextField from '@material-ui/core/TextField';
import {Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles';
import '../styles/mainStyle.css'
import {postLogin} from '../api/postApi'
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography'
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress'

import { validateLoginData } from '../util/validator'

const Login = (props) => {

    const classes = useStyles();
    const [emailValue, setEmailValue] = useState("")
    const [passwordValue, setPasswordValue] = useState("")
    const [error, setError] = useState({})
    const [isLoading, setIsLoading] = useState(false)

    const MyButton = (props) => {
        const { color, ...other } = props;
        const classes = useStyles(props);
        return <Button type='submit' variant='contained' className={classes.button} {...other} />;
    }

    MyButton.propTypes = {
        color: PropTypes.oneOf(['blue', 'red']).isRequired,
    };
    

    const onBtnClick = (e) => {
        e.preventDefault()
        
        const { valid, errors } = validateLoginData( emailValue, passwordValue )

        setIsLoading(true)
        if(valid){
            
            console.log("firebase is called?")
            try {
                axios.post('https://us-central1-qrcode-4b0f5.cloudfunctions.net/api/login', {
                    email: emailValue,
                    password: passwordValue
                })
                .then(res => {
                    localStorage.setItem('userIdToekn', res.data.token)
                    console.log(res.data)
                    setIsLoading(false)
                    props.history.push('/home?' + 'nameOfPlace=' + res.data.data.nameOfPlace + ' ' + res.data.data.uid)
                    console.log("[End] login")
                })
                .catch(e => {
                    console.log("+_+_+_+___+_+_+")
                    console.error(e.response.data)
                    setError(e.response.data)
                    setIsLoading(false)

                })
                   
            } catch(e) {
                console.error("error= ", e);
            }

        } else {
            setError(errors)
            setIsLoading(false)
        }

    }

    const login = 
    <div className="mainBox">
            <div style={{width:"100%", display:"flex", flexDirection:"row", flexWrap:"nowrap", justifyContent:"center", alignItems:"center", flexDirection: 'column'}}>
                
                {/* <Typography variant="h1">
                    Login
                </Typography> */}
                
                <Grid>
                    <TextField 
                        marginRight='100'
                        label="Email"
                        value={emailValue}
                        onChange={(e) => setEmailValue(e.target.value)}
                        id="emailValue"
                        helperText={error.email}
                        error={error.email ? true : false}
                        variant='standard'
                        className={classes.textField}
                        FormHelperTextProps=
                            {{ style: {color: 'blue'} }}
                    />
                    {/* <label> 
                        Name: <input type="text" name="name" placeholder="Full Name" onChange={(e)=>setNameValue(e.target.value)} />
                    </label> */}
                </Grid>
                <Grid>
                    <TextField 
                        marginRight='100'
                        label="Password"
                        value={passwordValue}
                        onChange={(e) => setPasswordValue(e.target.value)}
                        id="passwordValue"
                        helperText={error.password}
                        error={error.password ? true : false}
                        variant='standard'
                        className={classes.textField}
                        FormHelperTextProps=
                            {{ style: {color: 'blue'} }}
                    />
                    {/* <label>
                        Phone: <input type="text" name="phoneNum" placeholder="xxx-xxx-xxxx" onChange={(e)=>setPhoneNum(e.target.value)}/>
                    </label> */}
                </Grid>
            </div>

            <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                {error.general && (
                    <Typography variant="body2" className={classes.customError}>
                    {error.general}
                </Typography>
                )}
                <MyButton style={{marginTop: error.general ? 10 : 20,}} disabled={isLoading} onClick={onBtnClick} color="red">
                    Login
                    {isLoading && (
                        <CircularProgress size={70} className={classes.progress}/>
                    )}
                </MyButton>
                <small>dont have an accout ? <Link to="/signup">sign up</Link> </small>
                {/* <button className="submitBtn" onClick={postNameList(nameValue, phoneNum)}>
                    Submit
                </button> */}
            </div>
    </div>

    return login;
}


const useStyles = makeStyles((theme) => ({
    container: {
      height: "calc(100vh - 80px)",
      width: "100%",
      flexWrap: 'wrap',
      display: 'grid',
      justifyContent: 'center',
      alignItems: 'center',
      direction: 'row'
    },
    button: {
        background: 'linear-gradient(45deg, #33c9dc 30%, #039be5 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(51, 201, 220 .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: 8,
        position: 'relative',
        width:'191px',
        
      },
    textField: {
        width: '250px',
    },
    customError:{
        color: 'red',
        fontsize: '0.8rem',
        marginTop: 10,
    },
    progress: {
        position: 'absolute'
    }
  }));

export default Login