import React, { Component, useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid'
import '../styles/nameList.css'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import PropTypes from 'prop-types';
import {enqueue, dequeueByIndex} from '../api/postApi';
import {getQueue, dequeue} from '../api/getApi.js';

import { useLocation  } from 'react-router-dom'

const EnterQueue = (props) => {
    const classes = useStyles();
    const [userName, setUserName] = useState("")
    const [userPhone, setUserPhone] = useState("")
    const [numberOfPeople, setNumberOfPeople] = useState('')
    
    const location = useLocation()
    const urlPath = location.search
    const [param, setParam] = useState("")
    const [uid, setUid] = useState("")
    const [nameOfPlace, setNameOfPlace] = useState("")

    useEffect(()=>{
        console.log(urlPath)
        console.log("@#$%@#$^@#$%#@$%@#$%#@%#$%@$#%")
        setParam(new URLSearchParams(urlPath).get('nameOfPlace'))
        
    },[])
    
    useEffect(()=>{
        if(param != null && param != "") {
            console.log("!#!#!@#", param)
            const splitedParam = param.split(' ')
            setUid(splitedParam[1])
            setNameOfPlace(splitedParam[0])
            console.log("uid === ", splitedParam[1])
            console.log("nameOfPlace === ", splitedParam[0])
        }
    }, [param])
    
    const MyButton = (props) => {
        const { color, ...other } = props;
        const classes = useStyles(props);
        return <Button className={classes.button} {...other} />;
    }
    MyButton.propTypes = {
        color: PropTypes.oneOf(['blue', 'red']).isRequired,
    };

    const onBtnClick = async  () => {
        if(userName == "") 
        {
            alert("Name of Place should be filled!")
        } 
        else
        {
            let data = await enqueue(userName, userPhone, numberOfPeople, uid)
            console.log('data', data);
        }
    }

    const handleViewMode = () =>{
        
        return(
            <Grid className={classes.container} >
                <Grid>
                    <Grid item>
                        <TextField 
                            marginRight='100'
                            label="Your Name"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            id="nameOfPlace"
                            helperText="*Required"
                            variant='standard'
                            className={classes.textField}
                            FormHelperTextProps=
                                {{ style: {color: 'blue'} }}
                        />
                    </Grid>
                    <Grid item >
                        <TextField
                            label="Your Phone Number"
                            value={userPhone}
                            onChange={(e) => setUserPhone(e.target.value)}
                            id="tableNum"
                            helperText="*Required"
                            className={classes.textField}
                            FormHelperTextProps=
                                {{ style: {color: 'blue'} }}
                        />
                    </Grid>
                    <Grid item >
                        <TextField
                            label="Number of People (1 by default)"
                            value={numberOfPeople}
                            onChange={(e) => setNumberOfPeople(e.target.value)}
                            id="tableNum"
                            className={classes.textField}
                        />
                    </Grid>
                    <MyButton onClick={onBtnClick} color="red">Enter to queue</MyButton>
                </Grid>
            </Grid>
        );
    }
    
    return (
        handleViewMode()
    )
}

const useStyles = makeStyles((theme) => ({
    container: {
      height: "calc(100vh - 80px)",
      width: "100%",
      flexWrap: 'wrap',
      display: 'grid',
      justifyContent: 'center',
      alignItems: 'center',
      direction: 'row'
    },
    button: {
        background: 'linear-gradient(45deg, #33c9dc 30%, #039be5 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(51, 201, 220 .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: 8,
      },
    textField: {
        width: '300px'
    }
  }));

export default EnterQueue;