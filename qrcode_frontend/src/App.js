import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import createTheme from '@material-ui/core/styles/createMuiTheme'

import NavBar from './components/NavBar'
import Register from './pages/register'
import Qrcode from './pages/Qrcode'
import NameList from './pages/NameList'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import Login from './pages/Login'
import Signup from './pages/Signup'
import WaitList from './pages/WaitList'
import EnterQueue from './pages/EnterQueue'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#33c9dc',
      main: '#00bcd4',
      dark: '#008394',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ff6333',
      main: '#ff3d00',
      dark: '#b22a00',
      contrastText: '#fff'
    }
  }
})

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <div className="App">
        <Router>
        <NavBar className/>
          <div className="container">
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/signup" component={Signup} />
              <Route path="/myqrcode" component={Qrcode} />
              <Route path="/register" component={Register} />
              <Route path="/namelist" component={NameList} />
              <Route path="/waitlist" component={WaitList} />
              <Route path="/enterqueue" component={EnterQueue} />
            </Switch>
          </div>
        </Router>
      </div>
      </MuiThemeProvider>
  );
}

export default App;
