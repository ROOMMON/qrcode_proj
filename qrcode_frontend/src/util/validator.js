const isEmpty = (string) => {
    if(string.trim() === '') return true;
    else return false;
}

const isEmail = (email) => {
    const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.match(regEx)) return true;
    else return false;
}

export const validateSignupData = (email, password, confirmPassword, nameOfPlace) => {
    let errors = {}

    if(isEmpty(email)) {
        errors.email = 'Email should not be empty'
    } else if(!isEmail(email)) {
        errors.email = 'Must be a valid email address'
    }

    if(isEmpty(password)) errors.password = 'Password should not be empty'
    if(password !== confirmPassword) errors.confirmPassword = 'Password not match'
    if(isEmpty(confirmPassword)) errors.confirmPassword = 'Must not be empty'
    if(isEmpty(nameOfPlace)) errors.nameOfPlace = 'Place should not be empty'

    return {
        errors,
        valid: Object.keys(errors).length === 0 ? true : false
    }
}

export const validateLoginData = (email, password) => {
    let errors = {}

    if(isEmpty(email) && isEmpty(password)) {
        errors.email = "email should not be empty!"
        errors.password = "password should not be empty!"
    } else if(isEmpty(email)) {
        errors.email = "email should not be empty!"
    } else if(isEmpty(password)) {
        errors.password = "password should not be empty!"
    }

    // if(isEmpty(email)) errors.email = 'Must not be empty'
    // if(isEmpty(password)) errors.password = 'Must not be empty'

    return {
        errors,
        valid: Object.keys(errors).length === 0 ? true : false
    }
}