import React, { Component, useState, useEffect } from 'react'
// MUI 
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import ViewListIcon from '@material-ui/icons/ViewList';

import { BrowserRouter as Router, useHistory, Link, useLocation  } from 'react-router-dom'

import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/core/styles';

import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

//Drawer
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import CropFreeIcon from '@material-ui/icons/CropFree';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    list: {
        width: 250,
      },
      fullList: {
        width: 'auto',
      },
  }));

const NavBar = (props) => {
    let location = useLocation();
    
    const classes = useStyles();
    const history = useHistory();

    let urlPath = location.search
    // const param = new URLSearchParams(urlPath)
    // const queryParam = param.get('nameOfPlace').split(" ");
    // const nameOfPlace = queryParam[0]
    // const uid = queryParam[1]

    const [path, setPath] = useState(location.pathname)
    const [pathParam, setPathParam] = useState()
    const [auth, setAuth] = useState(false);
    const [authText, setAuthText] = useState('')
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const [isOpen, setIsOpen] = useState(false)
  
    useEffect(()=>{
        setPath(location.pathname)
        if(auth) { setAuthText('Logout')} else { setAuthText('Login') }
    })
    const handleMenu = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };

    const handleDrawerBtn = (text, e) => {
        if(text === 'Login') {
            setPath('/login' + urlPath)
            history.push('/login' + urlPath);
        } else if(text === 'Home') {
            setPath('/home' + urlPath)
            history.push('/home' + urlPath);
        } else if(text === 'Name List') {
            setPath('/namelist' + urlPath)
            history.push('/namelist' + urlPath);
        } else if(text === 'Wait List') {
            setPath('/waitlist' + urlPath)
            history.push('/waitlist' + urlPath)
        } else if(text === 'My QRcode') {
            setPath('/myqrcode' + urlPath)
            history.push('/myqrcode' + urlPath)
        }
    }

    const diplayIcon = (text) => {
        if(text === 'Logout') {
            return <AccountCircle />
        } else if(text === 'Home') {
            return <HomeIcon/>
        } else if(text === 'Name List') {
            return <ViewListIcon/>
        } else if(text === 'Wait List') {
            return <ViewListIcon />
        } else if(text === 'My QRcode') {
            return <CropFreeIcon />
        }
    }

    const displayheader = () => {
        if(path.startsWith('/login')) {
            return 'LogIn'
        } else if(path.startsWith('/home')) {
            return 'HOME'
        } else if(path.startsWith('/namelist')) {
            return 'Name List'
        } else if(path.startsWith('/signup')) {
            return 'SignUp'
        } else if(path.startsWith('/waitlist')) {
            return 'Wait List'
        } else if(path.startsWith('/myqrcode')) {
            return 'My QRcode'
        }
    }
    
    const toggleDrawer = (open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
    
        setIsOpen(open)
      };
    
      const list = () => (
        <div
          className={clsx(classes.list)}
          role="presentation"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer( false)}
        >
          <List>
            {[authText].map((text, index) => (
              <ListItem button key={text} onClick={(e)=>handleDrawerBtn(text, e)}>
                {auth ? <ListItemIcon> {diplayIcon(text)} </ListItemIcon> : null}
                {/* <ListItemIcon>{index % 2 === 0 ? (<InboxIcon /> ): <MailIcon />}</ListItemIcon> */}
                <ListItemText primary={text} /> 
              </ListItem>
            ))}
          </List>
          <Divider />
          <List>
            {['Home', 'Wait List', 'Name List', 'My QRcode'].map((text, index) => (
              <ListItem button key={text} onClick={(e)=>handleDrawerBtn(text, e)}>
                <ListItemIcon>{diplayIcon(text)}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
        </div>
      );

    return (
        <div className={classes.root}>
            <AppBar position="static" >
                <Toolbar >
                {/* <div  onClick={toggleDrawer(true)}>
                    <IconButton  edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon />    
                    </IconButton>
                </div> */}
                    <Drawer anchor={"left"} open={isOpen} onClose={toggleDrawer(false)}>
                        {list()}
                    </Drawer>
                    <Typography variant="h6" className={classes.title}>
                        {displayheader()}
                    </Typography>
                    {auth && (
                            <div>
                            
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                                }}
                                open={open}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={handleClose}>Profile</MenuItem>
                                <MenuItem onClick={handleClose}>My account</MenuItem>
                            </Menu>
                            </div>
                     )}
                    {/* <Button color="inherit" component={Link} to="/login">
                        Login
                    </Button>
                    <Button color="inherit" component={Link} to="/register">
                        Register Name
                    </Button>
                    <Button color="inherit" component={Link} to="/qrcode">
                        Generate QRcode
                    </Button>
                    <Button color="inherit" component={Link} to="/namelist">
                        Check Name List
                    </Button> */}
                </Toolbar>
            </AppBar>
        </div>
    )

}

export default NavBar;