const functions = require('firebase-functions');

const express = require('express')
const app = express();

const cors = require('cors');
const { userSignUp, userLogin, getUserData, postUserData } = require('./handlers/user');
const {  postEnqueue, getQueue } = require('./handlers/queue')
app.use(cors())


app.get('/userData', getUserData)

app.post('/userData', postUserData)

//Signup route
app.post('/signup', userSignUp)

app.post('/login', userLogin)

//Handle queue
app.post('/enqueue',postEnqueue)

// app.post('/dequeue', postDequeue)

app.get('/queue', getQueue)

// app.get('/dequeue', getDequeue)


exports.api = functions.https.onRequest(app)
