const { admin, db } = require('../util/admin')

const config = require('../util/config')

const firebase = require('firebase')
firebase.initializeApp(config)
// firebase.analytics();

const { validateSignupData, validateLoginData } = require('../util/validator')

exports.getUserData = (req, res) => {

    // db
    //     .collection('restaurants')
    //     .doc

    return db
            .collection('restaurants')
            .doc(req.query.uid)
            .get()
            .then((doc)=>{
                let dataSet = [];
                // doc.forEach(doc=>{
                //     dataSet.push(doc.data());
                // })
                return res.status(200).json(doc.data().visitors);
            })
            .catch(e => {
                console.error(e)
                return res.status(500).json({ error: e.code })
            });
}

exports.postUserData = (req, res) => {
    
    let uid = req.query.uid
    // let numOfGroupValue = req.body.numOfGroup
    // let tableNumValue = req.body.tableNum
    // let detailValue = req.body.detail
    
    console.log("uid=",uid)
    const newVisitors = [];
    req.body.visitors.map(value => {

        Object.assign(value, {
            // numOfGroup: numOfGroupValue,
            // tableNum: tableNumValue,
            // detail: detailValue,
            dateOfVisited: new Date().toISOString()
        })

        newVisitors.push(value)
    })
    
    const dataSet = {
        // id: req.body.id,
        // nameOfPlace: req.body.nameOfPlace,
        visitors: newVisitors
            // return res.json(req.body.users)
    }
    // Object.assign(target, source);
    return db
        .collection('restaurants')
        .doc(uid)
        .get()
        .then((doc)=>{
            
                // if(doc.data().users.length != 0){
                    const visitorArray = dataSet.visitors.concat(doc.data().visitors);
                        visitorArray.sort((a, b)=> a.dateOfVisited - b.dateOfVisited)
                        Object.assign(dataSet, {visitors: visitorArray})
                // }
                
        })
        .then(() => {
            return db
                        .collection("restaurants")
                        .doc(uid)
                        .update(dataSet)
                        .then(() => {
                            return res.status(201).json( { message: dataSet})
                        })
                        .catch(e => {
                            console.error(e);
                            return res.statusMessage(500).json({ error: dataSet})
                        }) 
        })
        .catch((e)=>{
            
            console.error(e)
            return res.status(500).json({ error: dataSet })
        })
}

exports.userSignUp = async (req, res) => {

    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword,
        nameOfPlace: req.body.nameOfPlace
    }

    const { valid, errors } = validateSignupData( newUser )

    if(!valid) return res.status(400).json({errors})

    //TODO: validate data
    let token, uid, user;

    firebase
        .auth()
        .createUserWithEmailAndPassword(newUser.email, newUser.password)
        .then((data) => {
            user = data.user
            uid = data.user.uid
            return data.user.getIdToken()
        })
        .then((idToken) => {
            token = idToken

            if(user){

                 db
                        .collection('restaurants')
                        .doc(uid)
                        .set(
                            {
                                uid,
                                nameOfPlace: newUser.nameOfPlace,
                                dateOfRegister: admin.firestore.FieldValue.serverTimestamp(), 
                                visitors: [],
                                queue: [],
                                dequeued: []
                            }
                        )
                        .then(()=>{
                            return res.status(201).json({ token })
                        })
            } 
        })
        .catch(e => {
            console.error(e)
            if (e.code == 'auth/email-already-in-use') {
                return res.status(400).json({ email: 'Email is already in use'})
            } else {
                return res.status(500).json({ error: e.code })
            }
        })

}

exports.userLogin = (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password
    }
    let token;
    let uid;
    let nameOfPlace;

    const { valid, errors } = validateLoginData( user )

    if(!valid) return res.status(400).json({errors})

    firebase.auth().signInWithEmailAndPassword(user.email, user.password)
        .then(data => {
            uid = data.user.uid
            return data.user.getIdToken()
        })
        .then((idToken) => {
            token = idToken
        })
        .then(()=>{
            console.log("1+_+_+_+_+_+_++_+_+_+_+_+++_+")
            db
            .collection('restaurants')
            .doc(uid)
            .get()
            .then((doc)=>{
                if(doc.exists) {
                    console.log("+_+_+_+_+_+_++_+_+_+_+_+++_+")
                    return res.status(201).json({ 
                        token,
                        data: doc.data() 
                    })
                } else {
                    console.log("No data is found")
                }
            })
        })
        .catch((e) => {
            console.error(e)
            // auth/user-not-found can be used too
            if(e.code === 'auth/wrong-password' || e.code === 'auth/invalid-email') {
                return res.status(403).json({ general: 'invalid Email or Password.'})
            } else return res.status(500).json({ general: 'Something wrong, please try again' })
        })

}
