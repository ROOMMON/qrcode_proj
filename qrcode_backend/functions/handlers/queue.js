const { admin, db } = require('../util/admin')

// firebase.analytics();

// const getQueueData = async (uid) =>{
//     let queue;
//     await db
//     .collection("restaurants")
//     .doc(uid)
//     .get()
//     .then((doc) =>{
//         queue = doc.data().queue
//     });
//     if(!Array.isArray(queue))
//         queue = [];
//     return queue;
// }

// const updateCollection = async (uid, update={}) =>{
//     const resp = await db.collection("restaurants")
//     .doc(uid).update(update);
//     return resp;
// }

// const getDequeued = async (nameOfPlace) =>{
//     let dequeued;
//     await db.collection("restaurants")
//     .where("nameOfPlace", "==", nameOfPlace).get()
//     .then((querySnapshot)=>querySnapshot.forEach(doc=> 
//         dequeued = doc.data().dequeued
//         ));
//     if(!Array.isArray(dequeued))
//         dequeued = [];
//     return dequeued;
// }

// const enqueue = async (uid, user={}) =>{
//     let queue = await getQueueData(uid);
//     queue.push({...user, enqueueDate: Date.now()});
//     const resp = await updateCollection(uid, {queue:queue});
//     return({
//         queue: queue,
//     });
// }

// const removeUserFromQueue = (queue, index=null) => {
//     let dequeuedUser;
//     if(index !== null)
//     {
//         dequeuedUser = queue.splice(index, 1);
//     }
//     else
//     {
//         dequeuedUser = queue.shift();
//     }
//     if(!dequeuedUser)
//         return null;
//     dequeuedUser = {...dequeuedUser, dequeueDate: Date.now()};
//     return dequeuedUser;
// };

// const dequeue = async (uid, index=null) =>{
//     // let dequeued = await getDequeued(nameOfPlace);
//     // console.log('dequeued',dequeued)
//     let queue = await getQueueData(uid);
//     const removedUser = removeUserFromQueue(queue, index);
//     if(removedUser)
//         dequeued.push(removedUser);
//     const resp = await updateCollection(
//         uid, 
//         {queue:queue, dequeued:dequeued}
//     );
//     return({
//         queue: queue,
//         dequeued: dequeued
//     });
// }
exports.postEnqueue = async (req, res) => {
    const uid = req.query.uid
    const name = req.body.name;
    const phone = req.body.phone;
    const numberOfPeople = req.body.numberOfPeople;
    const user = {name:name, phone:phone, numberOfPeople:numberOfPeople};
    let queue
    // const enq = await enqueue(uid, user);
    return db
        .collection("restaurants")
        .doc(uid)
        .get()
        .then((doc) =>{
            queue = doc.data().queue
        })
        .then(()=>{
            queue.push({...user, enqueueDate: Date.now()});
        })
        .then(()=>{
            return db
            .collection("restaurants")
            .doc(uid)
            .update({queue})
            .then(() => {
                return res.status(201).json(queue)
            })
            .catch((e) => {
                return res.status(500).json({message: "Error while updating wait list"})
            })
        })
        .catch((e)=>{
            
            console.error(e)
            return res.status(500).json({ error: e.code })
        })

}

// exports.postDequeue = async (req, res) => {
//     const uid = req.query.uid
//     const index = req.body.index;
//     console.log(`Dequeue from ${uid} queue`);
//     const dq = await dequeue(uid, index)
//     res.send(dq)
// }

// exports.getDequeue = async (req, res) => {
//     const nameOfPlace = req.query.nameOfPlace;
//     const uid = req.query.uid
//     console.log(`Dequeue from ${nameOfPlace} queue`);
//     const dq = await dequeue(nameOfPlace)
//     res.send(dq)
// }

exports.getQueue = (req, res) => {
    const uid = req.query.uid
    console.log(`Get ${uid} queue`);

    return db
        .collection("restaurants")
        .doc(uid)
        .get()
        .then((doc) =>{
            return res.status(200).json(doc.data().queue);
        })
        .catch((e)=>{
            
            console.error(e)
            return res.status(500).json({ error: e.code })
        })
}