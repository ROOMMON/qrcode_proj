const { admin } = require('./admin')

module.exports = (req, res, next) => {
    
    let idToekn
    if(req.headers.authorization && req.headers.authorization.startsWith('')) {
        idToken = req.headers.authorization.split('Bearer ')[1]
    } else {
        console.error('No token found')
        return res.status(403).json({ error: 'Unauthorized' })
    }

    admin.auth().verifyIdToken(idToken)
        .then(decodedToekn => {
            req.user = decodedToken
        })
}